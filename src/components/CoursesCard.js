import {useState} from 'react'
import {Row, Col, Card, Button} from 'react-bootstrap'

export default function CourseCard({courseProp}){
	console.log(courseProp)

	// object desctructuring 
	const {name, description, price} = courseProp
	// Syntax: const {properties} = propname

	// array destructuring
	const [count, setCount] = useState(0)
	const [seat, seatCount] = useState(30)
	console.log(useState(0))
	// Syntax: const [getter, setter] = useState(initialValue)

	// Hook used is useState - to store the state

	function enroll(){
		setCount(count + 1);
		seatCount(seat - 1);
		if(seat === 0){
			seatCount(0)
			setCount(30)
			alert('No more seats')
		}
		console.log('Enrollees' + count)
		console.log('Seats' + seat)
	}

	return(
		<Row className="mt-3 mb-5">
			<Col>
				<Card>
					<Card.Body>
						<Card.Title><h5>{name}</h5></Card.Title>
						<Card.Subtitle>Description</Card.Subtitle>
						<Card.Text>{description}</Card.Text>
						<Card.Subtitle>Price</Card.Subtitle>
						<Card.Text>{price}</Card.Text>
						<Card.Text>Enrollees: {count}</Card.Text>
						<Card.Text>Seats: {seat}</Card.Text>
						<Button variant="primary" onClick={enroll}>Enroll</Button>
					</Card.Body>
				</Card>
			</Col>
		</Row>
	)
}
